# Object Oriented Design

### Language

* JavaScript
* ES5

### Tool to use

* Node.js

### Structure

* **designpatterns** find relevant examples here
* **oop** find relevant examples here
* **task** complete task to practice OOP and DP
